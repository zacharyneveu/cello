from datetime import time, timedelta
from typing import List, Tuple

import uuid

from Block import Block


class Event:
    """
    Stores all info related to a Cello event
    Defaults to broad defaults if values not specified (i.e. defaults to all days of the week)
    """

    def __init__(
        self,
        name: str,
        days_of_week: List[str] = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
        time_range: Tuple[time, time] = (time(0, 0), time(23, 59)),
        duration: timedelta = timedelta(minutes=60),
        location: str = "",
        description: str = "",
        blocks: List[Block] = [],
    ) -> None:
        if not isinstance(name, str):
            raise TypeError("Name must be a string")
        if not isinstance(days_of_week, list):
            raise TypeError("Days of week must be a list")
        if not all(isinstance(day, str) for day in days_of_week):
            raise TypeError("Days of week must be strings")
        if not isinstance(time_range, tuple):
            raise TypeError("Time range must be a tuple of two time objects")
        if not all(
            isinstance(time_value, time)
            for time_value in (time_range[0], time_range[1])
        ):
            raise TypeError("Time range must be a tuple of two time objects")
        if not isinstance(duration, timedelta):
            raise TypeError("Duration must be a timedelta object")
        if not isinstance(location, str):
            raise TypeError("Location must be a string")
        if not isinstance(description, str):
            raise TypeError("Description must be a string")

        self.name = name
        self.days_of_week = days_of_week
        self.time_range = time_range
        self.duration = duration
        self.location = location
        self.description = description
        self.id = str(uuid.uuid4())
        self.blocks = blocks

    def __str__(self) -> str:
        days_str = ", ".join(self.days_of_week)
        time_str = f"{self.time_range[0]} - {self.time_range[1]}"
        duration_str = f"{self.duration.seconds // 60} minutes"
        description_str = self.description.replace("\n", "\n\t\t")
        return f"""
        {self.name}
        Days of week: {days_str}
        Time range: {time_str}
        Duration: {duration_str}
        Location: {self.location}
        Description: 
        \t\t{description_str}
        Scheduled Blocks: {self.blocks}
        """
