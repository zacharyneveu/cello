from typing import List, Dict, Tuple, Union, Optional
from google.oauth2 import credentials
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
import os

import datetime
from datetime import time, timedelta

from Event import Event
from TimeSlot import TimeSlot, get_free_slots
from Utils import get_creds
from Block import Block


class Calendar:
    def __init__(
        self,
        creds: credentials.credentials,
        timezone: str = "US/Eastern",
        cal_name: str = "primary",
        max_time_days: int = 7,
    ) -> None:
        self._service = build("calendar", "v3", credentials=creds)
        self._timezone: datetime.timezone = datetime.timezone(
            timedelta(minutes=0), timezone
        )
        self._cal_name: str = cal_name
        self._max_time_days: int = max_time_days
        self.free_times: List[TimeSlot]
        self.check_cal_free_times()

    def check_cal_free_times(self) -> None:
        date_start: str = (
            datetime.datetime.utcnow().isoformat() + "Z"
        )  # 'Z' indicates UTC time
        date_end: str = (
            datetime.datetime.utcnow() + datetime.timedelta(days=self._max_time_days)
        ).isoformat() + "Z"

        # Define a list of calendars to check for free/busy times
        calendar_list_entry: Dict[str, str] = {"id": self._cal_name}
        calendar_list: List[Dict[str, str]] = [calendar_list_entry]

        # Send the request to the API to get free-busy times
        freebusy_query: Dict[str, Union[str, List]] = {
            "timeMin": date_start,
            "timeMax": date_end,
            "timeZone": self._timezone.tzname(None),
            "items": calendar_list,
        }
        freebusy = self._service.freebusy().query(body=freebusy_query).execute()

        # Extract the free-busy times from the response
        bts: List[Dict[str, str]] = freebusy["calendars"][self._cal_name]["busy"]
        print(bts)
        busy_times: List[TimeSlot] = [
            TimeSlot.from_strings(bt["start"], bt["end"], "%Y-%m-%dT%H:%M:%S%z")
            for bt in bts
        ]
        start_t = datetime.datetime.now().astimezone(self._timezone)
        end_t = datetime.datetime.now().astimezone(self._timezone) + timedelta(
            days=self._max_time_days
        )
        self.free_times = get_free_slots(busy_times, start_t, end_t)
        print("free times: ")
        for ft in self.free_times:
            print(ft)

    def find_time_on_day(self, event: Event, date: datetime.date) -> List[TimeSlot]:
        """Find all time slots that are free on the calendar and meet the event constraints"""
        eligible_time_slots: List[TimeSlot] = []
        st, et = event.time_range
        start_time = datetime.datetime.combine(date, st).astimezone(self._timezone)
        end_time = datetime.datetime.combine(date, et).astimezone(self._timezone)
        while (start_time + event.duration) <= end_time:
            proposed_ts = TimeSlot(start_time, start_time + event.duration)
            # check if proposed time fits into any of the free times
            # exit early if
            for ft in self.free_times:
                if proposed_ts in ft:
                    eligible_time_slots.append(proposed_ts)
                    break

            start_time += timedelta(minutes=30)
        return eligible_time_slots

    def find_day_and_time(self, event: Event) -> List[TimeSlot]:
        """Find all times on all days when calendar is free and event constraints are met"""
        # Define the date range for free-busy query

        # Loop through the list of events and find available timeslots for scheduling
        eligible_time_slots: List[TimeSlot] = []
        for i in range(self._max_time_days):
            day = datetime.datetime.today().astimezone(
                self._timezone
            ) + datetime.timedelta(days=i)
            day_otw = day.strftime("%a")
            if day_otw in event.days_of_week:
                eligible_time_slots.extend(self.find_time_on_day(event, day))
        return eligible_time_slots

    def schedule(self, event: Event, guest_email: str) -> Optional[Event]:
        self.check_cal_free_times()
        # Use the get_free_slots function here to get eligible time slots for the event
        free_slots = self.find_day_and_time(event)

        # Check if there are any eligible time slots
        if free_slots:
            # Select the first eligible time slot
            chosen_slot = free_slots[0]

            # Create a new event on the calendar
            cal_event = {
                "summary": event.name,
                "location": event.location,
                "description": event.description,
                "start": {
                    "dateTime": chosen_slot.start.isoformat(),
                },
                "end": {
                    "dateTime": chosen_slot.end.isoformat(),
                },
                "visibility": "public",
                # TODO: figure out access to invite user email to event on service account calendar
                "attendees": [
                    {"email": guest_email},
                ],
            }
            created_event = (
                self._service.events()
                .insert(calendarId=guest_email, body=cal_event)
                .execute()
            )
            print(f'Event scheduled: {created_event["htmlLink"]}')
            chosen_slot.link = created_event["htmlLink"]
            block: Block = Block(event.id, [chosen_slot])
            event.blocks.append(block)
            return event
        else:
            print("No available time slots for scheduling.")
            return None


def main():
    creds = get_creds("./client-secret.json", "./client-credentials.json")

    calendar = Calendar(
        creds=creds,
        timezone="US/Eastern",
        cal_name="zachary.neveu@gmail.com",
        max_time_days=14,
    )

    # Define the list of events with their respective constraints
    event1 = Event(
        "Event 1",
        ["Mon", "Wed"],
        (time(10, 0), time(22, 30)),
        timedelta(minutes=30),
        "Location A",
    )
    event2 = Event(
        "Event 2",
        ["Tue", "Thu"],
        (time(14, 0), time(23, 0)),
        timedelta(minutes=60),
        "Location B",
    )
    event3 = Event(
        "Event 3",
        ["Mon", "Tue"],
        (time(13, 0), time(15, 30)),
        timedelta(minutes=90),
        "Location C",
    )

    events_list = [event1]

    for event in events_list:
        event_available_slots = calendar.find_day_and_time(event)
        if event_available_slots:
            print(
                f"First Available time slot for {event.name}: {event_available_slots[0]}"
            )
        else:
            print(f"No time found for event: {event.name}")
        calendar.schedule(event, "zachary.neveu@gmail.com")


if __name__ == "__main__":
    main()
