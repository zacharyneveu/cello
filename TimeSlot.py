from datetime import datetime, timedelta, timezone
from typing import List


class TimeSlot:
    def __init__(
        self,
        start: datetime = datetime.now(),
        end: datetime = datetime.now(),
        link: str = "",
    ) -> None:
        if end < start:
            raise ValueError("End time cannot be before start time")
        self.start = start
        self.end = end
        self.link = link

    @classmethod
    def from_strings(
        self, start: str, end: str, fmt: str, link: str = ""
    ) -> "TimeSlot":
        start_dt = datetime.strptime(start, fmt)
        end_dt = datetime.strptime(end, fmt)
        if end_dt < start_dt:
            raise ValueError("End time cannot be before start time")
        return TimeSlot(start_dt, end_dt, link)

    def __str__(self) -> str:
        duration = self.duration()
        return f"{self.start.strftime('%m-%d %I:%M %p %Z')} - {self.end.strftime('%m-%d %I:%M %p %Z')}"

    def __contains__(self, other: "TimeSlot") -> bool:
        return self.start <= other.start and self.end >= other.end

    def duration(self) -> timedelta:
        return self.end - self.start

    def astimezone(self, tz: timezone):
        if not self.start:
            raise RuntimeError("Start time not found")
        if not self.end:
            raise RuntimeError("end time not found")

        self.start = self.start.astimezone(tz)
        self.end = self.end.astimezone(tz)


def get_free_slots(
    slots: List[TimeSlot], start: datetime, end: datetime
) -> List[TimeSlot]:
    """
    Given a list of busy time slots, return a list of time slots not selected in slots between
    start and end.
    """
    if not slots:
        return [TimeSlot(start, end)]

    # initialize an empty list to store free slots
    free_slots = []

    # the first free slot is from the start of the day to the start time
    if slots and slots[0].start > start:
        free_slots.append(TimeSlot(start, slots[0].start))

    # find free slots between the selected time slots
    for i in range(1, len(slots)):
        if slots[i - 1].end < slots[i].start:
            free_slots.append(TimeSlot(slots[i - 1].end, slots[i].start))

    # the last free slot is from the end time to the end of the day
    if slots and slots[-1].end < end:
        free_slots.append(TimeSlot(slots[-1].end, end))

    return free_slots
