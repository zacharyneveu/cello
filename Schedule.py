#!/usr/bin/env python

from datetime import time, timedelta
from dateutil import parser
from Calendar import Calendar
from Event import Event
from typing import List, Tuple

from Utils import get_creds, get_days, get_duration, get_time


def get_event_info() -> Event:
    name: str = input("Enter the name of the event: ")
    days_of_week: str = input(
        "Enter the days of the week for the event (comma separated): "
    )
    days: List[str] = get_days(days_of_week)
    start_time: time = get_time(
        input("Enter the start time of the event (hh:mm AM/PM or any format): ")
    )
    end_time: time = get_time(
        input("Enter the end time of the event (hh:mm AM/PM or any format): ")
    )
    duration: timedelta = get_duration(
        input("Enter the duration of the event in minutes or any duration format: ")
    )
    location: str = input("Enter the location of the event: ")
    return Event(name, days, (start_time, end_time), duration, location)


def main() -> None:
    creds = get_creds("./client-secret.json", "./client-credentials.json")
    calendar: Calendar = Calendar(
        creds=creds,
        timezone="US/Eastern",
        cal_name="zachary.neveu@gmail.com",
        max_time_days=7,
    )

    # Get event info and add to the events list
    event: Event = get_event_info()
    print(event)

    calendar.schedule(event, "zachary.neveu@gmail.com")


if __name__ == "__main__":
    main()
