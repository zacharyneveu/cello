from TimeSlot import TimeSlot
from typing import List
import uuid


class Block:
    def __init__(self, parent_uuid: str, timeslots: List[TimeSlot] = []):
        self.id = str(uuid.uuid4())
        self.parent_uuid = parent_uuid
        self.slots = timeslots

    def __str__(self) -> str:
        return " ".join([ts.__str__() for ts in self.slots])
