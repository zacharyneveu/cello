from flask import (
    Flask,
    request,
    jsonify,
    Response,
    redirect,
    url_for,
    render_template,
    session,
)
from flask_dance.contrib.google import make_google_blueprint, google
from flask_session import Session  # type: ignore
from google.oauth2 import credentials
from datetime import time, timedelta
from dateutil import parser
from typing import Dict, List, Tuple
from Event import Event
from Calendar import Calendar
import os
from werkzeug.middleware.proxy_fix import ProxyFix

from Utils import get_duration, get_time

app = Flask(__name__)
app.secret_key = "subaru"
app.config["SESSION_TYPE"] = "filesystem"
app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_prefix=1)  # type: ignore
Session(app)

# create a blueprint that will handle the authentication flow
blueprint = make_google_blueprint(
    client_id=os.environ.get("GOOGLE_CLIENT_ID"),
    client_secret=os.environ.get("GOOGLE_CLIENT_SECRET"),
    scope=[
        "openid",
        "https://www.googleapis.com/auth/userinfo.email",
        "https://www.googleapis.com/auth/userinfo.profile",
        "https://www.googleapis.com/auth/calendar",
        "https://www.googleapis.com/auth/calendar.events",
    ],
    offline=True,
    reprompt_consent=False,
)


# register the blueprint with the application
app.register_blueprint(blueprint, url_prefix="/login")


@app.route("/")
def index():
    # if the user is not authenticated yet, redirect them to the Google authentication page
    if not google.authorized:
        return redirect(url_for("google.login"))

    # fetch the user's info from their Google account
    resp = google.get("/oauth2/v2/userinfo")
    assert resp.ok, resp.text
    session["user-email"] = resp.json()["email"]
    session["user-name"] = resp.json()["name"]

    creds = credentials.Credentials.from_authorized_user_info(
        info={
            "access_token": google.token["access_token"],
            "refresh_token": google.token["refresh_token"],
            "client_id": os.environ.get("GOOGLE_CLIENT_ID"),
            "client_secret": os.environ.get("GOOGLE_CLIENT_SECRET"),
        }
    )

    if not "calendar" in session.keys():
        session["calendar"]: Calendar = Calendar(
            creds=creds,
            timezone="US/Eastern",
            cal_name=session["user-email"],
            max_time_days=7,
        )
        print("Calendar created")

    return render_template("index.html")


@app.route("/viewEvents", methods=["GET"])
def view_events():
    events = session.get("events", {})
    print(events)
    return render_template("view_events.html", events=events)


@app.route("/newEventForm", methods=["GET"])
def new_event_form():
    return render_template("trello.html")


@app.route("/schedule/<event_id>", methods=["GET"])
def schedule(event_id):
    try:
        if "events" not in session.keys():
            return ("No events created yet, create an event first using /addEvent", 400)
        if event_id not in session["events"].keys():
            return (f"Event with ID {event_id} not found, try a different ID", 400)
        session["calendar"].schedule(
            session["events"][event_id], guest_email=session["user-email"]
        )
        return redirect(url_for("success", event_id=event_id))
    except Exception as e:
        return jsonify({"error": str(e)}), 400


@app.route("/success/<event_id>")
def success(event_id):
    if "events" not in session.keys():
        return ("No events created yet, create an event first using /addEvent", 400)
    if event_id not in session["events"].keys():
        return (f"Event with ID {event_id} not found, try a different ID", 400)
    event = session["events"][event_id]
    [print(t) for t in event.blocks]
    return render_template("success.html", event=event)


@app.route("/addEvent", methods=["POST"])
def add_event():
    try:
        if not google.authorized:
            return redirect(url_for("google.login"))
        data: Dict[str, str] = request.form.to_dict()
        name: str = data.get("name", "")
        days_of_week: List[str] = [
            d
            for d in ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
            if d in data.keys()
        ]
        start_time: time = get_time(data["start_time"])
        end_time: time = get_time(data["end_time"])
        duration: timedelta = get_duration(data["duration"])
        location: str = data.get("location", "")
        description: str = data.get("description", "")

        event = Event(
            name=name,
            days_of_week=days_of_week,
            time_range=(start_time, end_time),
            duration=duration,
            location=location,
            description=description,
        )

        print(event)

        cal_data = session.get("calendar")
        if session["calendar"]:
            event = session["calendar"].schedule(
                event, guest_email=session["user-email"]
            )
            if event:
                if not "events" in session.keys():
                    session["events"] = {}
                session["events"][event.id] = event
                return redirect(url_for("view_events"))
            else:
                return ("<bf>No time slot found :(</bf>", 200)
        else:
            return jsonify({"error": "calendar not initialized"}), 400
        return jsonify({"success": "Event added successfully."}), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 400


if __name__ == "__main__":
    app.run(debug=True)
