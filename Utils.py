import os
from google.oauth2.credentials import Credentials, credentials
from google_auth_oauthlib.flow import InstalledAppFlow
import google.auth.exceptions
from datetime import time, timedelta
from dateutil import parser
from typing import List


def get_creds(secret_path: str, creds_path: str) -> credentials:
    scopes = [
        "https://www.googleapis.com/auth/calendar.events",
        "https://www.googleapis.com/auth/calendar",
    ]

    if os.path.exists(creds_path):
        creds = Credentials.from_authorized_user_file(creds_path, scopes)

    else:
        # Set up the Google Authentication flow
        try:
            flow = InstalledAppFlow.from_client_secrets_file(secret_path, scopes)
        except google.auth.exceptions.DefaultCredentialsError:
            print(
                'No credentials found. Please run the "gcloud init" command and sign in with your Google account.'
            )
            exit()

        # Prompt the user to authenticate
        print("Please authenticate to proceed.")
        creds = flow.run_local_server()

        # Save the credentials for future use
        os.makedirs(os.path.dirname(creds_path), exist_ok=True)
        with open(creds_path, "w") as f:
            f.write(creds.to_json())

        # Use the credentials to make API requests
        creds = Credentials.from_authorized_user_file(creds_path, scopes)
    return creds


def get_time(input_time: str) -> time:
    try:
        return parser.parse(input_time).time()
    except ValueError:
        print("Could not parse time")
        return time(0, 0)


def get_duration(input_duration: str) -> timedelta:
    try:
        return timedelta(minutes=int(input_duration))
    except ValueError:
        duration = parser.parse(input_duration) - parser.parse("00:00")
        return timedelta(minutes=duration.seconds // 60)


def get_days(days_input: str) -> List[str]:
    days = [parser.parse(day).strftime("%a") for day in days_input.split(",")]
    return days
